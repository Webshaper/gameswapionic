'use strict';

/**
 * @ngdoc function
 * @name gameSwapApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the gameSwapApp
 */
angular.module('gameSwapApp')
  .controller('NewsCtrl', function ($scope, $ionicModal) {
    $scope.news = [
      {
        created: '2015-03-01 12:24:53',
        headline: 'HTC - Virtual-Reality-Headset in Zusammenarbeit mit Valve Software angekündigt',
        description: 'HTC hat heute auf dem "Mobile World Congress" ein eigenes Virtual-Reality-Headset mit dem Titel "Re Vive" angekündigt, das in Zusammenarbeit mit Valve Software entwickelt wird.'+
        'Die im Headset verbauten Displays sollen pro Auge eine Auflösung von 1200x1080 Bildpunkten bieten - mit einer Bildwiederholfrequenz von 90 Hz. (Vergleich: Oculus Rift DK2: 960x1080 Pixel pro Auge mit 75 Hz). Dadurch soll die VR-Erfahrung besonders "angenehm" werden, so jedenfalls der Hersteller. Das Headset bietet zudem eine "Aussparung", um eigene Kopfhörer nutzen zu können.'+
        'Zur Erfassung der Kopfposition (Neigung, Drehung etc.) werden ein Beschleunigungs- und Lagesensor sowie ein Laser-Positionssensor verwendet. Laut HTC soll die Erfassung bis auf ein Zehntel Grad genau sein. Zudem kann das Headset mit zwei (nicht näher benannten) Basisstationen gekoppelt werden, um auch die Position der Person im Raum erfassen zu können. Man soll sich in einem knapp 4,5 Meter mal 4,5 Meter großen Areal bewegen können, ohne dass Kopf- und Positionsverfolgung abgebrochen werden. Bei der Präsentation hieß es, dass man sich in diesem Bereich in der "virtuellen Welt bewegen könne". Hinzu kommen zwei VR-Controller, deren Position und Ausrichtung ebenfalls verfolgt werden. Sie sollen die Manipulation bzw. Interaktion von Objekten in der virtuellen Welt erlauben.'+
        'Das Gerät wird SteamVR als Schnittstelle für Spiele und Programme verwenden - quasi eine Virtual-Reality-Erweiterung der Vertriebsplattform von Valve. Die Entwicklerversion soll im Frühling 2015 erscheinen. Ein Preis steht noch nicht fest. Die Comsumer-Variante soll Ende des Jahres folgen.',
        image: '36466-teaser1.png'
      },
      {
        created: '2015-03-02 13:24:53',
        headline: 'Test: The Order 1886',
        description: 'Exklusiv auf PlayStation 4 kann man ab Freitag in die Haut von Sir Galahad schlüpfen. Allerdings nicht mit Schwert und Lanze, sondern mit Revolver und Luftschiffen. The Order: 1886 entführt in ein alternatives viktorianisches London, in dem die Ritter der Tafelrunde immer noch gegen die Feinde Englands kämpfen. Kann die Action überzeugen? Mehr dazu im Test.'+
        'Ein Hoch auf die Kostüme!'+
        'Ich kann mich gar nicht satt sehen an dieser Mode. Da sind die düsteren, elegant geschnittenen Uniformen, deren Leder und Stoffe man aus der Schultersicht fast fühlen kann, weil sowohl Nähte als auch Oberflächen so fein dargestellt werden. Da schimmern silberne Beschläge mit eingravierten Symbolen neben poliertem Elfenbein oder mattgrauen Schießeisen im diffusen Licht der Kamera – sieht das klasse aus!'+
        'Und in Bewegung tanzt Sir Galahads tiefschwarzer Schulterbesatz mit den königlichen Stickereien: Das rote Kreuz auf weißem Feld, das ihn im alternativen  Jahr 1886 als englischen Ritter kennzeichnet. Er gehört der legendären Tafelrunde an, die nicht mit Artus untergegangen ist, sondern wie eine geheime Spezialeinheit über Jahrhunderte hindurch gegen moderne sowie uralte Feinde des Königreichs kämpft: Rebellen hier, Werwölfe da.'+
        'Mit Bezügen zur Gralslegende über soziale Unruhen in London bis hin zu Horror und Steampunk wird eine interessante Ausgangslage voller Motive geschaffen. Sehr früh wird auch Jack the Ripper erwähnt, der gerade eine Frau nach der  Die Story macht neugierig: Die Ritter der Tafelrunde agieren wie eine Spezialeinheit zum Schutze Englands…'+
        'Die Story macht neugierig: Die Ritter der Tafelrunde agieren wie eine Spezialeinheit zum Schutze Englands…'+
        'anderen tötet. Das ist zwar eine "elementare" Aufgabe für Sherlock Holmes und nicht für die Tafelrunde, aber die Regie lässt einen Schmelztiegel rund um den berüchtigten Bezirk Whitechapel bedrohlich dampfen. Wer zieht da die Fäden? Wie hängt das zusammen? Man ist neugierig, will sofort loslegen!'+
        'Als ich mit Sir Galahad, laut Sage der Sohn Lancelots, der als besonders edel, galant und rein gilt, über den verschachtelten Dächern Londons innehalte und über die Themse blicke, wo Schornsteine qualmen oder Luftschiffe wie Wale dahin gleiten, weckt dieser neo-viktorianische Schauplatz neben der Neugier auch die Entdeckerlust. Schnell wird jedoch klar: Für Erkunder einer mysteriösen Welt oder einer belebten Metropole ist dieses Abenteuer nicht gemacht – es geht um lineare Action, in der über acht Stunden von A nach B geschossen oder geschlichen wird.',
        image: '34845-teaser1.jpg'
      },
      {
        created: '2015-01-07 14:24:53',
        headline: 'Battlefield Hardline: Entwickler reagiert auf Kommentare, dass es sich gar nicht um ein Battlefield-Spiel handeln würde',
        description: 'Battlefield Hardline wird häufig als "kein richtiges Battlefield" oder als "große Modifikation von Battlefield 4" beschrieben. Und zu diesen Kommentaren, die es häufig während der offenen Beta-Phase gab, äußerte sich nun Mike Glosecki (Gameplay Systems Producer) von Entwickler Visceral Games und legte seine Sichtweise der Dinge dar.'+
        'Mike Glosecki: "Einige Spieler haben gesagt, dass sie das Spiel für Battlefield in neuer Verpackung halten. Am anderen Ende des Spektrums gab es jedoch auch Kommentare, dass es sich gar nicht um ein Battlefield-Spiel handeln würde. Ich möchte euch meine Sichtweise erklären. Bei Battlefield geht es um erbarmungslose Schlachten, um Teamplay und um Strategie. Hardline hat das alles zu bieten, und zwar in einer Cops-&-Gangster-Spielwelt. Das klassische Battlefield-Spielvergnügen ist mit dem Eroberungs-Modus für 64 Spieler auf unseren größeren Maps wie Staubwüste, Brandung, Entgleisung und Everglades enthalten. Darüber hinaus gibt es neue Gameplay-Modi wie Hotwire und Überfall. In Hotwire könnt ihr den Nervenkitzel von Verfolgungsjagden in der Battlefield-Welt erleben. Der Überfall-Modus versetzt euch mitten in ein Actionfilm-Spektakel. Ihr müsst Tresore knacken und euch zu einem Übergabepunkt vorkämpfen, wo ein Helikopter schon auf eure Beute wartet. Hardline erweitert das Battlefield-Erlebnis um neue Spielmechaniken wie Takedowns, mit denen ihr eure Gegner verhören und die Positionen ihrer Teamkollegen aufdecken könnt. Außerdem könnt ihr mit Medikits oder Munitionskisten ausgerüstete Mitspieler anklicken, um schnell eure Gesundheit oder eure Munition auf Vordermann zu bringen. Zudem gibt es in Hardline auch neue Ausrüstungsobjekte wie die Stungun (T62 CEW) oder die Nahkampf-Hiebwaffen. Mit Gadgets wie Seilrutschen, Kletterhaken oder Zielpfeilen dürften erfahrene Battlefield-Spieler ja bereits vertraut sein.'+
        'Es hat auch Kommentare gegeben, das Spiel würde sich zu schnell anfühlen, und Taktik und Teamplay würden dabei etwas auf der Strecke bleiben. Ich möchte betonen, dass es bei den Taktiken um Action geht. Es geht darum, auf bestimmte Situationen zu reagieren und die gewünschten Resultate zu erhalten. Bei Hardline gibt es jede Menge solcher Elemente. Beispielsweise, wenn ihr euch mit Positionsspiel und Situationserkennung an einen Gegner heranarbeitet. Das unterscheidet sich, was Methode und Skills angeht, von reiner Zielpräzision. Und es ist noch anspruchsvoller, da es im Vergleich zum reinen Zielen mehr auf kluges Überlegen ankommt. Teamplay ist ein großer Bestandteil unseres Spielerlebnisses. Zum Beispiel im Überfall-Modus. Wenn euch jemand den Rücken freihält oder die Gegner aus dem Weg räumt, sind eure Chancen, zu überleben und den Übergabepunkt zu erreichen, deutlich größer. Im Fadenkreuz-Modus müsst ihr als Team zusammenarbeiten, um euer VIP-Ziel zu beschützen und zu gewinnen. Im Hotwire-Modus steigen eure Fahrleistungen, wenn jemand euren Wagen unterwegs repariert und eure Kollegen dafür sorgen, dass euer Wagen nicht von einem Hubschrauber demoliert wird. Im Blood Money-Modus und dem Fadenkreuz-Modus bringt ebenfalls nur Teamwork den Sieg."',
        image: '35785-teaser1.jpg'
      }
    ];

    $scope.tasks = [
        { title: 'Collect coins' },
        { title: 'Eat mushrooms' },
        { title: 'Get high enough to grab the flag' },
        { title: 'Find the Princess' }
    ];

    $scope.games = [
        {
            title: 'LittleBigPlanet',
            platform: 'ps3'
        },
        {
            title: 'Battlefield 3',
            platform: 'ps3'
        },
        {
            title: 'Battlefield 4',
            platform: 'ps3'
        },
        {
            title: 'Grand Theft Auto IV',
            platform: 'xbox360'
        },
        {
            title: 'The Order 1886',
            platform: 'ps4'
        }
    ];

    // Create and load the Modal
    $ionicModal.fromTemplateUrl('offer.html', function(modal) {
        $scope.taskModal = modal;
    }, {
        scope: $scope,
        animation: 'slide-in-up'
    });

    $scope.newOffer = function() {
        //$scope.taskModal.show();
        //$document.location.href = '/offer';
    };
    $scope.closeNewTask = function() {
        $scope.taskModal.hide();
    };

    $scope.callbackMethod = function (query) {
        var matches = [];
        for(var i = 0; i < $scope.games.length; i++){
            if($scope.games[i].title.indexOf(query) > -1){
                matches.push($scope.games[i]);
            }
        }
        return matches;
    };


  });
