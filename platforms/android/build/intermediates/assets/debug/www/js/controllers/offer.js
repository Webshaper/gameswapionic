'use strict';

/**
 * @ngdoc function
 * @name gameSwapApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the gameSwapApp
 */
angular.module('gameSwapApp')
    .controller('OfferCtrl', function ($scope, $ionicModal) {


        $scope.games = [
            {
                title: 'LittleBigPlanet',
                platform: 'ps3'
            },
            {
                title: 'Battlefield 3',
                platform: 'ps3'
            },
            {
                title: 'Battlefield 4',
                platform: 'ps3'
            },
            {
                title: 'Grand Theft Auto IV',
                platform: 'xbox360'
            },
            {
                title: 'The Order 1886',
                platform: 'ps4'
            }
        ];
/*
        // Create and load the Modal
        $ionicModal.fromTemplateUrl('offer.html', function(modal) {
            $scope.taskModal = modal;
        }, {
            scope: $scope,
            animation: 'slide-in-up'
        });

        $scope.newOffer = function() {
            //$scope.taskModal.show();
            //$document.location.href = '/offer';
        };
        $scope.closeNewTask = function() {
            $scope.taskModal.hide();
        };*/

        $scope.callbackMethod = function (query) {
            var matches = [];
            for(var i = 0; i < $scope.games.length; i++){
                if($scope.games[i].title.indexOf(query) > -1){
                    matches.push($scope.games[i]);
                }
            }
            return matches;
        };


    });
